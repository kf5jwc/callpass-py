from datetime import datetime, timedelta

import pytest

from callpass.validated_callpass.cache_heuristics import OneHourHeuristic


@pytest.fixture
def heuristic():
    return OneHourHeuristic()


def test_invalid_header_date(heuristic):
    class Response:
        headers = {"date": None}

    ret = heuristic.update_headers(Response)

    assert ret["cache-control"] == "public"

    expected = datetime.now() + timedelta(hours=1)
    datestr = expected.strftime("%a, %d %b %Y %H")
    assert datestr in ret["expires"]


def test_warning(heuristic):
    assert '110 - "Automatically' in heuristic.warning(None)
