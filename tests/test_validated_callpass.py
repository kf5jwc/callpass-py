import pytest

import callpass
from callpass import (
    ExpiredLicense,
    InvalidLicense,
    MalformedCallsign,
    ServerUpdating,
    ValidatedCallpass,
)


def test_malformed_callsign(maintenance_mode):
    with pytest.raises(MalformedCallsign):
        ValidatedCallpass("AAAAAA")


###


@pytest.fixture()
def response_valid(monkeypatch):
    def valid(*_a, **_kwa):
        return {"status": "Valid", "otherInfo": {"expiryDate": "10/20/9999"}}

    monkeypatch.setattr(
        callpass.validated_callpass.validated_callpass,
        "fetch_license",
        valid,
    )


def test_valid_license(response_valid):
    callpass = ValidatedCallpass("AB3DEF")
    assert int(callpass) == 17570


###


@pytest.fixture()
def response_expired(monkeypatch):
    def expired(*_a, **_kwa):
        return {"status": "Valid", "otherInfo": {"expiryDate": "10/20/1999"}}

    monkeypatch.setattr(
        callpass.validated_callpass.validated_callpass,
        "fetch_license",
        expired,
    )


def test_expired_license(response_expired):
    with pytest.raises(ExpiredLicense):
        ValidatedCallpass("AB3DEF")


###


@pytest.fixture()
def response_invalid(monkeypatch):
    def invalid(*_a, **_kwa):
        return {"status": "Invalid"}

    monkeypatch.setattr(
        callpass.validated_callpass.validated_callpass,
        "fetch_license",
        invalid,
    )


def test_invalid_licence(response_invalid):
    with pytest.raises(InvalidLicense):
        ValidatedCallpass("AB3DEF")


###


@pytest.fixture()
def maintenance_mode(monkeypatch):
    def server_updating(_callpass: str) -> dict:
        raise ServerUpdating

    monkeypatch.setattr(
        callpass.validated_callpass.validated_callpass,
        "fetch_license",
        server_updating,
    )


def test_server_updating(maintenance_mode):
    with pytest.raises(ServerUpdating):
        ValidatedCallpass("AB3DEF")
