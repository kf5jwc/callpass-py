from json import dumps as json

import pytest

from callpass import ServerStatus, ServerUpdating
from callpass.validated_callpass import license_validation_tools


@pytest.fixture
def callook_http_not_200(monkeypatch):
    class Non200:
        status_code: int = 999

        def __call__(self, *_, **_kw):
            return self

    monkeypatch.setattr(license_validation_tools.REQUEST_SESSION, "get", Non200())


def test_http_non_200(callook_http_not_200):
    with pytest.raises(ServerStatus):
        license_validation_tools.fetch_license("AB3DEF")


###


CALLOOK_NONSTANDARD_STATUS = {"status": "Whee"}


@pytest.fixture
def callook_nonstandard_status(monkeypatch):
    class NonstandardStatus:
        status_code: int = 200
        text: str = json(CALLOOK_NONSTANDARD_STATUS)

        def __call__(self, *_, **_kw):
            return self

    monkeypatch.setattr(
        license_validation_tools.REQUEST_SESSION, "get", NonstandardStatus()
    )


def test_callook_nonstandard_status(callook_nonstandard_status):
    with pytest.raises(ServerStatus):
        license_validation_tools.fetch_license("AB3DEF")


###


CALLOOK_STATUS_UPDATING = {"status": "Updating"}


@pytest.fixture
def callook_status_updating(monkeypatch):
    class StatusUpdating:
        status_code: int = 200
        text: str = json(CALLOOK_STATUS_UPDATING)

        def __call__(self, *_, **_kw):
            return self

    monkeypatch.setattr(
        license_validation_tools.REQUEST_SESSION, "get", StatusUpdating()
    )


def test_callook_status_updating(callook_status_updating):
    with pytest.raises(ServerUpdating):
        license_validation_tools.fetch_license("AB3DEF")


###


CALLOOK_VALID_CURRENT = {"status": "Valid", "otherInfo": {"expiryDate": "10/20/9999"}}


@pytest.fixture
def callook_valid_current(monkeypatch):
    class Current:
        status_code: int = 200
        text: str = json(CALLOOK_VALID_CURRENT)

        def __call__(self, *_, **_kw):
            return self

    monkeypatch.setattr(license_validation_tools.REQUEST_SESSION, "get", Current())


def test_retrieval(callook_valid_current):
    ret = license_validation_tools.fetch_license("AB3DEF")
    assert ret == CALLOOK_VALID_CURRENT


###


CALLOOK_VALID_EXPIRED = {"status": "Valid", "otherInfo": {"expiryDate": "10/20/1999"}}


@pytest.fixture
def callook_valid_expired(monkeypatch):
    class Expired:
        status_code: int = 200
        text: str = json(CALLOOK_VALID_EXPIRED)

        def __call__(self, *_, **_kw):
            return self

    monkeypatch.setattr(license_validation_tools.REQUEST_SESSION, "get", Expired())


def test_valid_expired(callook_valid_expired):
    ret = license_validation_tools.fetch_license("AB3DEF")
    assert ret == CALLOOK_VALID_EXPIRED
