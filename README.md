APRS Callpass generator
=======================

What, why?
----------

This is a tiny thing I decided to build because I got tired of people waiting for other people to give them their APRS code. Some wait times reach weeks long.

This is a much faster solution which aims to remove emails and wait times. Less hassle for humans, less pissoff-ery for the person needing their code.

How do I use it?
------------------

```python
from callpass import Callpass

_ = Callpass("AB3DEF")  # Callpass(callsign="AB3DEF")
_ = int(Callpass("AB3DEF"))  # 17570
```

or

```python
from callpass import ValidatedCallpass

_ = ValidatedCallpass("AB3DEF")  # raises InvalidLicense
_ = int(ValidatedCallpass("AB3DEF"))  # raises InvalidLicense
```

When can I use it?
------------------

If you need a APRS passcode while you're offline, `Callpass`.

If you need to verify USA callsigns, `ValidatedCallpass`.

What can go wrong?
------------------

### `Callpass`

`Callpass` will return a code for anything, including garbage. Be careful with your input.

### `ValidatedCallpass` 

`ValidatedCallpass` will raise exceptions for various conditions:

- `ExpiredLicense` when a license is expired.
- `InvalidLicense` if the callsign does not exist.
- `MalformedCallsign` when the callsign does not match USA format.
- `ServerUpdating` when the lookup service is refreshing its database.
- `ServerStatus` when the lookup service is having server issues.

TODO
----

- Find a license data source which is more comprehensive than the FCC. ( Do NOT suggest QRZ )
- Doesn't currently identify cancelled licenses because I don't know if that has a consistent entry in the database.


CREDITS
-------

- xastir
	xastir (released under a compatible GPL license) is a cool peice of software.
	Thanks for the callpass hash method. I wouldn't have known where else to find it.

- callook
	callook (callook.info) is a (very) fast, well behaved amateur license database.
	Uses data from the FCC, but has vastly greater performance!


LICENSE
-------
	
GPLv3
